#!/usr/bin/env python
import json
import requests
import io
import base64
from errbot import BotPlugin, botcmd, cmdfilter
from twilio.rest import Client

class Sms(BotPlugin):
    """
    SMS Bot
    """

    def get_configuration_template(self):
        return {'TWILIO_ACCOUNT_SID': '00112233445566778899aabbccddeeff',
                'TWILIO_AUTH_TOKEN': '00112233445566778899aabbccddeeff',
                'TWILIO_OUR_NUMBER': '+614444444444'}

    @cmdfilter
    def committee_only(self, msg, cmd, args, dry_run):
        """
        :param msg: The original chat message.
        :param cmd: The command name itself.
        :param args: Arguments passed to the command.
        :param dry_run: True when this is a dry-run.
           Dry-runs are performed by certain commands (such as !help)
           to check whether a user is allowed to perform that command
           if they were to issue it. If dry_run is True then the plugin
           shouldn't actually do anything beyond returning whether the
           command is authorized or not.
        """

        if cmd.startswith('sms'):
            who = str(msg.frm)
            if who.startswith('@srw'):
                return msg, cmd, args
            elif who.startswith('~committee/'):
                return msg, cmd, args
            else:
                self.log.info(f'restricted command: {cmd} for {who}')
                return None, None, None

        return msg, cmd, args

    @botcmd
    def sms_send(self, msg, args):
        """
        send a message from the ballarat hackerspace text number to someone
        """
        
        if not self.config:
            yield f'The required twilio configuration has not been set, use: `!plugin config sms`'
            return

        try:
            account_sid = self.config['TWILIO_ACCOUNT_SID']
            auth_token = self.config['TWILIO_AUTH_TOKEN']
            client = Client(account_sid, auth_token)

            who = "unknown"
            if '/' in str(msg.frm):
                who = f'@{str(msg.frm).split("/")[1]}'
            else:
                who = msg.frm

            (sendto_raw, _, mesg) = args.partition(' ')
            if sendto_raw.startswith('+61'):
                sendto=sendto_raw
            elif sendto_raw.startswith('04'):
                sendto=f'+61{int(sendto_raw)}'
            else:
                yield f'{who} I do not recognise that as a mobile number'
                return

            message = client.messages.create(body=mesg,
                                             from_=self.config['TWILIO_OUR_NUMBER'],
                                             to=sendto)

            yield f'{who} your message to {sendto} has been sent ([twilio log](https://console.twilio.com/us1/monitor/logs/sms?sid={message.sid}))'
        except Exception as e:
            yield f'{who} sorry there was an error sending your sms: {e}'
